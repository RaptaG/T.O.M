<img src="icons/legacy_icon.png" alt="T.O.M's icon" width="150" align="right">

# T.O.M

<a href="https://modrinth.com/modpack/tom"><img alt="Available on Modrinth" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact/available/modrinth_vector.svg"></a>
<a href="https://discord.gg/mWymzAS5Ex"><img alt="Chat with us on Discord" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact/social/discord-plural_vector.svg"></a>

---

# Discontinued

I started taking a C++ course and I really have no will or time to continue maintaining this modpack, even though I like it.

So the statement is official: **T.O.M is depricated**

There is an extremely small chance I'll continue the project in future, but I doubt it. It's really a pain to update from version to version.

If you are willing, please fork the pack and carry the project, it's a shame.

Thanks for all the love I got! I love y'all <3

---

## 👥 Credits

Thanks to:

- [robotkoer](https://modrinth.com/user/robotkoer), for making the amazing Fabulously Optimized modpack
- **Every single developer** of all the mods and resource pack included in T.O.M
- [comp500](https://modrinth.com/user/comp500), for creating the amazing [packwiz](https://packwiz.infra.link) tool, which I use to maintain T.O.M
- [Modrinth](https://modrinth.com), for making this pack possible to be published to a greater Minecraft modding audience
- [Codeberg](https://codeberg.org), for hosting an open source, [Forgejo](https://forgejo.org) based git service where I host T.O.M
- My friend [Chistandr](https://github.com/Chistandr), for constantly asking me for my mod list
- Everybody who asked for my mod list, including [Zxhir](https://github.com/Imzxhir)

## 📜 License

This modpack is open source and licensed under the GNU LGPLv3 License.

Image assets inside the [`icons`](icons/) folder are licensed under the [Open Art License](https://three.org/openart/license/index.html), version 1.0
